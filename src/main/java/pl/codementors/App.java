package pl.codementors;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Random;

/**
 * Hello world!
 *
 */
public class App extends Application
{
    public static void main( String[] args )
    {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        // utworzenie grup
        Group root = new Group();

        //utworzenie figur
        Polygon polygon = new Polygon(200, 260, 400, 260, 450, 300, 400, 340, 200, 340, 150, 300);
        Text text = new Text("hello in JavaFX");
        Circle circle1 = new Circle(50);
        Circle circle2 = new Circle(50);
        Circle circle3 = new Circle(50);
        Circle circle4 = new Circle(50);
       // Rectangle rectangle1 = new Rectangle(100, 100);


        //ustawianie pozycji obiektów
        text.setX(250);
        text.setY(250);
        circle1.setCenterX(50);
        circle1.setCenterY(50);
        circle2.setCenterX(550);
        circle2.setCenterY(50);
        circle3.setCenterX(550);
        circle3.setCenterY(550);
        circle4.setCenterX(50);
        circle4.setCenterY(550);
       // rectangle1.setX(100);
       // rectangle1.setY(0);

        //kolorowanie figur
        polygon.setFill(Color.RED);
        text.setStroke(Color.GREEN);
        circle1.setFill(Color.BLUE);
        circle2.setFill(Color.BLUE);
        circle3.setFill(Color.BLUE);
        circle4.setFill(Color.BLUE);
       /* Random ran =  new Random();
        ran.nextDouble();
        rectangle1.setFill(Color.color());
        **/

        //dodanie obiektu do roota
        root.getChildren().add(polygon);
        root.getChildren().add(text);
        root.getChildren().add(circle1);
        root.getChildren().add(circle2);
        root.getChildren().add(circle3);
        root.getChildren().add(circle4);
        //root.getChildren().add(rectangle1);

        for (int i = 0; i< 8; i++){
            root.getChildren().add(printRectangle(50*i+100, 25));
            root.getChildren().add(printRectangle(25, 50*i+100));
            root.getChildren().add(printRectangle(525, 50*i+100));
            root.getChildren().add(printRectangle(50*i+100,525 ));
        }


        // utworzenie sceny
        Scene scene = new Scene(root, 600, 600,  Color.BLACK);

        //wyświetlenie
        stage.setScene(scene);
        stage.show();

    }
    static Rectangle printRectangle (int setX, int setY){
        Group root = new Group();
        Rectangle rectangle = new Rectangle(50, 50);
        Random ran =  new Random();
        ran.nextDouble();
        rectangle.setFill(Color.color(ran.nextDouble(), ran.nextDouble(), ran.nextDouble()));
        rectangle.setX(setX);
        rectangle.setY(setY);

        return rectangle;
    }
}
